import { LitElement, html } from '@polymer/lit-element';

export class MySubTitle extends LitElement {
  constructor() {
    super()
  }

  render(){
    return html`
      <style>
        .subtitle {
          font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif;
          font-weight: 300;
          font-size: 42px;
          color: #526488;
          word-spacing: 5px;
          padding-bottom: 15px;
        }      
      </style>
      <h2 class="subtitle">
        Training with 🦊 GitLab is fun
      </h2>    
    `
  }
}
customElements.define('my-sub-title', MySubTitle)