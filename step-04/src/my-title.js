import { LitElement, html, customElement, property} from '@polymer/lit-element'
import {style} from './main-styles.js';

/**
  Importing customElement, property
  I can use decorators and then simplify the source code
 */

import { channels } from './bus.js'


//@customElement('my-title')
export class MyTitle extends LitElement {
  // static properties?
  //@property( { type : String }  ) title = 'Kitchen Sink 🍔 [LitElement]';

  
  static get properties() {
    return {
        message: { attribute: 'message' }
    }
  }
  

  //@property( { attribute : 'message' }  ) message



  constructor() {
    super()
    this.title = 'Kitchen Sink 🍔 [LitElement]'
    //this.message = "👋"
    channels.push(this)
    console.log(channels)
    let messages = [
      '🍉',
      '🌭',
      '🥕'
    ]

    //⚠️ very important
    setInterval(()=> {
      //this.title = new Date() // cannot work because it's not a property except if you render a property
      // => change of properties triggers a render
      let i = Math.floor(Math.random() * 3)
      this.message = messages[i] // this work because this is a property
    }, 1000)

  }
  render(){
    return html`
      ${style}
      <h1 class="title">
        ${this.title} ${this.message}
      </h1> 
    `
  }

}
customElements.define('my-title', MyTitle)